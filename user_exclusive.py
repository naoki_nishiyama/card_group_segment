# - * - coding:utf - 8 - * -
try:
    from pyspark import SparkConf
    from pyspark import SparkContext
    conf = SparkConf().setMaster("yarn-cluster")\
                      .setAppName("cardgroup_segment")
    sc = SparkContext(conf=conf)
except Exception:
    sc = SparkContext()

'''************************************'''

import pandas as pd
import datetime

today_ = (datetime.datetime.now() + datetime.timedelta(hours=9)).date()
save_path = "wasb://analytics@prodsaisonstorage.blob.core.windows.net/saison_ml_bucket/"


# 指定された日付から遡って、ユーザテーブルを読み込み セゾンユーザ
def read_saison_user_table(today, path):  # todayはYYYYMMDD
    data = sc.textFile(path + "20160413_neo/")
    data = data.map(lambda line: line.split('\t')).filter(lambda col: col[0] == u'2').filter(lambda col: len(col) == 72)
    data = data.map(lambda a: a[:] + [datetime.datetime.strptime(u'20160413', '%Y%m%d').date()])

    d_range = pd.date_range("20160414", today)
    num = -1
    pair_date = d_range[num]
    while True:
        tmp_data = sc.textFile(path + str(pair_date.date()).replace("-", "") + "/")
        tmp_data = tmp_data.map(lambda line: line.split('\t'))\
                           .filter(lambda col: col[0] == u'2').filter(lambda col: len(col) == 72)
        tmp_data = tmp_data.map(lambda a: a[:] + [pair_date.date()])
        try:
            data = data.union(tmp_data)
        except:
            pass
        num -= 1
        if -len(d_range) > num:
            break
        else:
            pair_date = d_range[num]
    return data


# 指定された日付から遡って、ユーザテーブルを読み込み UCユーザ
def read_uc_user_table(today, path):   # todayはYYYYMMDD
    d_range = pd.date_range("20160413", today)
    data = sc.parallelize([])
    num = -1
    pair_date = d_range[num]
    while True:
        tmp_data = sc.textFile(path + str(pair_date.date()).replace("-", "") + "/")
        tmp_data = tmp_data.map(lambda line: line.split('\t')).filter(lambda col: col[0] == u'2')\
            .filter(lambda col: len(col) == 42)
        tmp_data = tmp_data.map(lambda a: a[:] + [datetime.datetime.strptime(u'20160415', '%Y%m%d')])
        try:
            data = data.union(tmp_data)
        except:
            pass
        num -= 1
        if -len(d_range) > num:
            break
        else:
            pair_date = d_range[num]
    return data


def main():
    # セグメント定義リストをロード
    segment_master = pd.read_csv('card_segment_master.txt', header=None, index_col=0)

    # ユーザテーブルをMKDGからロード
    data_saison = read_saison_user_table(today_, "wasb:///MKDG1001/")

    # SIDでまとめて、最新のレコードだけ残す
    user_data_saison = data_saison.map(lambda col: (col[1], col[2:])).groupByKey().map(lambda x: [x[0], list(x[1])])
    unique_user_saison = user_data_saison.mapValues(lambda v: sorted(v, key=lambda x: x[-1])).mapValues(lambda v: v[-1])

    # まずは必要なカラムを取り出し
    unique_user2_saison = unique_user_saison.mapValues(lambda col: [col[20], col[32]])

    exclusive_user_saison = unique_user2_saison.filter(lambda col:
                                                       col[1][0] == u'06' or col[1][0] == u'07' or col[1][0] == u'08' or
                                                       col[1][1] == u'0' or col[1][1] == u'2')


    # ここからUCユーザ
    data_uc = read_uc_user_table(today_, "wasb:///MKDG1002/")

    # SIDでまとめて、最新のレコードだけ残す
    user_data_uc = data_uc.map(lambda col: (col[1], col[2:])).groupByKey().map(lambda x: [x[0], list(x[1])])
    unique_user_uc = user_data_uc.mapValues(lambda v: sorted(v, key=lambda x: x[-1])).mapValues(lambda v: v[-1])

    # まずは必要なカラムを取り出し
    unique_user2_uc = unique_user_uc.mapValues(lambda col: [col[9], col[26]])

    exclusive_user_uc = unique_user2_uc.filter(lambda col:
                                               col[1][0] == u'2' or col[1][0] == u'3' or
                                               col[1][1] == u'0' or col[1][1] == u'2')

    exclusive_user = exclusive_user_saison.union(exclusive_user_uc)

    # blobに保存、簡易csv的にパーティションを１つに
    pick_user_ = exclusive_user.map(lambda col: col[0].encode("utf-8"))
    pick_user = pick_user_.repartition(1)
    pick_user.saveAsTextFile(save_path + 'card_segment/' + str(today_).replace("-", "") + "/exclusive")


if __name__ == "__main__":
    main()
